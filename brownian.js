inlets = 1;
outlets = 1;

var note_range = 21; //0 to note_range, inclusive
var step_range = 2;  //one to step_range, inclusive
var current_note = 0;

if(jsarguments.length > 1)
  note_range = jsarguments[1];

if(jsarguments.length > 2)
  step_range = jsarguments[2];

function bang()
{
  outlet(0, current_note);

  var step = 1 + Math.floor(Math.random() * Math.floor(step_range));
  var is_down;
  if((current_note + step) > note_range)
    is_down = 1;
  else if((current_note - step) < 0)
    is_down = 0;
  else 
    is_down = Math.floor(Math.random() * Math.floor(2));

  if(is_down == 1)
     step = - step;
  
  current_note += step;
}
