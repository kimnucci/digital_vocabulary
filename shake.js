inlets = 1

var running = 0;

function msg_int(n)
{
  msg_float(n);
}

function msg_float(n)
{
  if(n < 0)
    n=0;

  if (n > running)
     running = n;
  else
    running *= 0.97;// * samnple_interval;

  if(running > 1)
    running = 1;   

  if(running < 0)
    running = 0; 

  outlet(0, Math.sqrt(running));
  
}