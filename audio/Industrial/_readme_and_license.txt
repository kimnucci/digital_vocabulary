Sound pack downloaded from Freesound
----------------------------------------

This pack of sounds contains sounds by the following user:
 - phoenixdk ( https://freesound.org/people/phoenixdk/ )

You can find this pack online at: https://freesound.org/people/phoenixdk/packs/5238/

License details
---------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/


Sounds in this pack
-------------------

  * 80435__phoenixdk__pssssssssccchhhhhh.wav
    * url: https://freesound.org/s/80435/
    * license: Creative Commons 0
  * 80434__phoenixdk__psch.wav
    * url: https://freesound.org/s/80434/
    * license: Creative Commons 0
  * 80433__phoenixdk__prprprpr.wav
    * url: https://freesound.org/s/80433/
    * license: Creative Commons 0
  * 80432__phoenixdk__plrom2.wav
    * url: https://freesound.org/s/80432/
    * license: Creative Commons 0
  * 80431__phoenixdk__plrom.wav
    * url: https://freesound.org/s/80431/
    * license: Creative Commons 0
  * 80430__phoenixdk__plonk_plonk.wav
    * url: https://freesound.org/s/80430/
    * license: Creative Commons 0
  * 80429__phoenixdk__plom.wav
    * url: https://freesound.org/s/80429/
    * license: Creative Commons 0
  * 80428__phoenixdk__ploing2.wav
    * url: https://freesound.org/s/80428/
    * license: Creative Commons 0
  * 80427__phoenixdk__ploing.wav
    * url: https://freesound.org/s/80427/
    * license: Creative Commons 0
  * 80426__phoenixdk__plinnnnnnnng.wav
    * url: https://freesound.org/s/80426/
    * license: Creative Commons 0
  * 80425__phoenixdk__plank_plank.wav
    * url: https://freesound.org/s/80425/
    * license: Creative Commons 0
  * 80424__phoenixdk__plang.wav
    * url: https://freesound.org/s/80424/
    * license: Creative Commons 0
  * 80423__phoenixdk__pisch.wav
    * url: https://freesound.org/s/80423/
    * license: Creative Commons 0
  * 80422__phoenixdk__klir_klir.wav
    * url: https://freesound.org/s/80422/
    * license: Creative Commons 0
  * 80421__phoenixdk__klir_klir_2.wav
    * url: https://freesound.org/s/80421/
    * license: Creative Commons 0
  * 80420__phoenixdk__klik_bom_bonnnng.wav
    * url: https://freesound.org/s/80420/
    * license: Creative Commons 0
  * 80419__phoenixdk__donnng2.wav
    * url: https://freesound.org/s/80419/
    * license: Creative Commons 0
  * 80418__phoenixdk__donnng.wav
    * url: https://freesound.org/s/80418/
    * license: Creative Commons 0
  * 80417__phoenixdk__donk.wav
    * url: https://freesound.org/s/80417/
    * license: Creative Commons 0
  * 80416__phoenixdk__crsschh.wav
    * url: https://freesound.org/s/80416/
    * license: Creative Commons 0
  * 80415__phoenixdk__crsschh_2.wav
    * url: https://freesound.org/s/80415/
    * license: Creative Commons 0
  * 80414__phoenixdk__crash.wav
    * url: https://freesound.org/s/80414/
    * license: Creative Commons 0
  * 80413__phoenixdk__crash_klir.wav
    * url: https://freesound.org/s/80413/
    * license: Creative Commons 0
  * 80412__phoenixdk__crash_klir_2.wav
    * url: https://freesound.org/s/80412/
    * license: Creative Commons 0
  * 80411__phoenixdk__crash_klinke.wav
    * url: https://freesound.org/s/80411/
    * license: Creative Commons 0
  * 80410__phoenixdk__crash_brom_brom.wav
    * url: https://freesound.org/s/80410/
    * license: Creative Commons 0
  * 80409__phoenixdk__cranch.wav
    * url: https://freesound.org/s/80409/
    * license: Creative Commons 0
  * 80408__phoenixdk__bum.wav
    * url: https://freesound.org/s/80408/
    * license: Creative Commons 0
  * 80407__phoenixdk__bonk.wav
    * url: https://freesound.org/s/80407/
    * license: Creative Commons 0
  * 80405__phoenixdk__bong_bom.wav
    * url: https://freesound.org/s/80405/
    * license: Creative Commons 0
  * 80406__phoenixdk__bong.wav
    * url: https://freesound.org/s/80406/
    * license: Creative Commons 0
  * 80404__phoenixdk__bom.wav
    * url: https://freesound.org/s/80404/
    * license: Creative Commons 0
  * 80403__phoenixdk__bom_klir.wav
    * url: https://freesound.org/s/80403/
    * license: Creative Commons 0
  * 80402__phoenixdk__blom.wav
    * url: https://freesound.org/s/80402/
    * license: Creative Commons 0


