inlets = 2; //sample_interval, gyro_y
outlets = 1;

var g = [inlets];


//var maximum = 2 * Math.PI;
var maximum = 5 * Math.PI;

if(jsarguments[1])
  maximum = jsarguments[1];

var minimum = -0.8
var orientation = maximum;
var previous    = maximum;
var deg_2_rad = Math.PI / 360.0;

function clear()
{
  orientation = minimum;
  previous    = minimum;
}

function msg_int(n)
{
	msg_float(n);
}

function msg_float(n)
{
	g[inlet] = n;
	
	if(inlet == 0)
	  { 
		g[1] *= deg_2_rad;
        previous += g[1]; //gyro
        //previous *= 0.5 * g[0]; //sample_interval
        previous *= g[0]; //sample_interval
        orientation += previous;
        previous = g[1];

        if(orientation > maximum)
           orientation = maximum;
        if(orientation < minimum)
           orientation = minimum;

        outlet(0, orientation);
      }
}
